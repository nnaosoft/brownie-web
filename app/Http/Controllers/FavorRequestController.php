<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\FavorRequest;
// use App\Models\User;
// use App\Models\EventParticipant;
use App\Http\Requests\RequestFavorRequest;
// use Imageupload;
/**
 * @Resource("Challenges", uri="/challenges" )
 */
class FavorRequestController extends Controller
{

    /**
     * List of challenges
     *
     * @Get("/")
     * 
     * @Parameters({
     *      @Parameter("title", description="Search by title of challenge "),
     *      @Parameter("status", type="integer", description="1= new challenges , 3= completed challenges, default completed listing show"),
     *      @Parameter("type", type="integer", description="1= REPLY, 2 = LIVE, if leave it both result return"),
     *      @Parameter("user_id", type="integer", description="Get user completed or new challenges"),
     *      @Parameter("tags", type="array", description="Search by tags of challenge")
     * })
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":{{"id":2,"title":"Challenge Two","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:42","tags":{"100","Pushups"},"likes":2,"liked":true, "flagged": false,"challenger_video":{"id":2,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":1,"comment":null,"deleted_at":null,"created_at":"2017-04-18 12:58:43","updated_at":"2017-04-18 12:58:43","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}},"defender_video":{"id":4,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":2,"comment":null,"deleted_at":null,"created_at":"2017-04-19 07:15:18","updated_at":"2017-04-19 07:15:18","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}}}}})
     * })
     */
    public function index(Request $request)
    {
        die("asdasd");
        $user = Auth::user();
        $favors = FavorRequest::latest();
        $type = $request->get('type', false);
        if ($type) {
            $favors->where('type', '=', $type);
        }
        $city_id = $request->get('city_id', false);
        if ($city_id) {
            $favors->where('city_id', '=', $city_id);
        }
        $friend_id = $request->get('friend_id', false);
        if ($friend_id) {
            $favors->where('friend_id', '=', $friend_id);
        }
       
        return $favors->paginate(20);
    }

 
    public function store(RequestFavorRequest $request)
    {
        $favor_request = new FavorRequest($request->all());
        $favor_request->doer_id = Auth::user()->id;
        if ($favor_request->save()) {
            return $favor_request;
        }
        if ($favor_request->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not request a favor.', $favor_request->getErrors());
        }

        throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not request a favor.', $favor_request->getErrors());
        
    }

}
