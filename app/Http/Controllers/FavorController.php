<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Favor;
use App\Models\FavorRequest;
use App\Models\User;
use App\Models\EventParticipant;
use App\Http\Requests\AddFavorRequest;
use Imageupload;
/**
 * @Resource("Challenges", uri="/challenges" )
 */
class FavorController extends Controller
{

    /**
     * List of challenges
     *
     * @Get("/")
     * 
     * @Parameters({
     *      @Parameter("title", description="Search by title of challenge "),
     *      @Parameter("status", type="integer", description="1= new challenges , 3= completed challenges, default completed listing show"),
     *      @Parameter("type", type="integer", description="1= REPLY, 2 = LIVE, if leave it both result return"),
     *      @Parameter("user_id", type="integer", description="Get user completed or new challenges"),
     *      @Parameter("tags", type="array", description="Search by tags of challenge")
     * })
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":{{"id":2,"title":"Challenge Two","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:42","tags":{"100","Pushups"},"likes":2,"liked":true, "flagged": false,"challenger_video":{"id":2,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":1,"comment":null,"deleted_at":null,"created_at":"2017-04-18 12:58:43","updated_at":"2017-04-18 12:58:43","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}},"defender_video":{"id":4,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":2,"comment":null,"deleted_at":null,"created_at":"2017-04-19 07:15:18","updated_at":"2017-04-19 07:15:18","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}}}}})
     * })
     */
    public function index(Request $request, $listing_type = 'normal')
    {
        $user = Auth::user();
        // $status = $request->get('status', Event::STATUS_COMPLETED);
        // $events = Event::with('challengerVideo', 'defenderVideo')
        //     ->status($status);
        // $sms = SMS::whereUser($user_id);
        
        // $limit = $request->get('limit', false);
        // if ($limit) {
        //     $sms->limit($limit);
        // }
        // $field = $request->get('field', 'created_at');
        // $order = $request->get('order', 'asc');
        // return $sms->orderBy($field, $order)
        //         ->paginate(20);
        $favors = Favor::with([
            'user'
        ])->latest();
        $type = $request->get('type', false);
        if ($type) {
            $favors->where('type', '=', $type);
        }
        $city_id = $request->get('city_id', false);
        if ($city_id) {
            $favors->where('city_id', '=', $city_id);
        }
        $friend_id = $request->get('friend_id', false);
        if ($friend_id) {
            $favors->where('friend_id', '=', $friend_id);
        }
        $favors->where('status', '=', Favor::STATUS_PENDING);
        // $title = $request->get('title', false);
        // if ($title) {
        //     $events->where('title', 'like', '%' . $title . '%');
        // }
        // $tags = $request->get('tags', []);
        // if (count($tags)) {
        //     $events->withAnyTag($tags);
        // }

        // $user_id = $request->get('user_id', false);
        // if ($listing_type == 'timeline') {
        //     if ($user_id && $user_id != $user->id) {
        //         $user = User::findOrFail($user_id);
        //     }
        //     $friends = $user->getAcceptedFriendships();
        //     $user_ids = [];
        //     foreach ($friends as $key => $friend) {
        //         $user_ids[] = $friend->sender_id;
        //         $user_ids[] = $friend->recipient_id;
        //     }
        //     $user_ids = array_unique($user_ids);
        //     $pos = array_search($user->id, $user_ids);
        //     unset($user_ids[$pos]);
        //     $events->whereHas('participants', function($query) use ($user_ids) {
        //         $query->whereIn('user_id', $user_ids);
        //     });
        // } elseif ($user_id) {
        //     $events->whereHas('participants', function($query) use ($user_id) {
        //         $query->where('user_id', '=', $user_id);
        //     });
        // }
        // $favors->latest();
        return $favors->paginate(20);
    }

    public function favorsAddedByMe(Request $request){
        $user = Auth::user();
        $favors = Favor::latest();
        $status = $request->get('status', false);
        $review = $request->get('review', false);
        $favors->where('user_id', '=', $user->id);
        if ($status) {
            if ($status == Favor::STATUS_COMPLETED && $review) {
                $favors->where('rating', '=', null);
            }
            $favors->where('status', '=', $status);            
        }
        return $favors->paginate(20);
    }

    public function favorsDoneByMe(Request $request){
        $user = Auth::user();
        $favors = Favor::latest();
        $status = $request->get('status', false);
        // $review = $request->get('review', false);
        // $friend_id = $request->get('friend_id', false);
        if ($status == Favor::STATUS_PENDING) {
            $favors->where('friend_id', '=', $user->id);
        }else{
            $favors->where('doer_id', '=', $user->id);
        }
        $favors->where('status', '=', $status);
        return $favors->paginate(20);
    }

    

    /**
     * Friends Challenges
     *
     * @Get("/timeline")
     * 
     * @Parameters({
     *      @Parameter("status", type="integer", description="1= new challenges , 3= completed challenges, default completed listing show")
     * })
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":{{"id":2,"title":"Challenge Two","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:42","tags":{"100","Pushups"},"likes":2,"liked":true, "flagged": false,"challenger_video":{"id":2,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":1,"comment":null,"deleted_at":null,"created_at":"2017-04-18 12:58:43","updated_at":"2017-04-18 12:58:43","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}},"defender_video":{"id":4,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":2,"comment":null,"deleted_at":null,"created_at":"2017-04-19 07:15:18","updated_at":"2017-04-19 07:15:18","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}}}}})
     * })
     */
    public function timeline(Request $request)
    {
        return $this->index($request, 'timeline');
    }

    /**
     * Create challenge
     *
     * @Post("/")
     * 
     * @Parameters({
     *      @Parameter("title", description="Challenge title", required=true),
     *      @Parameter("type", type="integer", description="1= REPLY, 2 = LIVE", required=true),
     *      @Parameter("description", description="Challenge description"),
     *      @Parameter("tags", type="array", description="tags of challenge"),
     *      @Parameter("video_url", description="Videos url on AWS S3 required only for reply challenge", required=true)
     * })
     * 
     * @Transaction({
     *      @Request({ "title": "User One", "type": 1, "description": "I am professioal Builder", "video_url": "http://gangster-strength.local.com", "tags": {"pushups", "100"} }, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"challenge":{"title":"User One","type":1,"description":"I am professioal Builder","video_url":"http:\/\/gangster-strength.local.com","user_id":27,"uploader":1,"created_at":"2017-04-13 10:24:32","id":2,"tags":{"Pushups","100"}}}),
     *      @Response(422, body={"message":"Could not add Challenge.","errors":{"video_url":{"The video url field is required."}},"status_code":422})
     * })
     */
    public function store(AddFavorRequest $request)
    {
        $favor = new Favor($request->all());
        $favor->user_id = Auth::user()->id;
        if ($request->hasFile('image')) {
            // $image_upload = Imageupload::upload($request->file('image'));
            // $old_avatar = $profile->avatar;
            //if ($image_upload['dimensions']['square250']) {
                // $profile->avatar = $image_upload['dimensions']['square250']['filedir'];
            //} else {
                $path = $request->file('image')->storePublicly('favors', ['disk' => 'uploads']);
                // var_dump(123);die;
                $favor->image = 'uploads/' . $path;
            //}
            if ($favor->save()) {
                return $favor;
            }
            // return $profile;
        }
        if ($favor->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not add favor.', $favor->getErrors());
        }

        throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not add favor.', $favor->getErrors());
        
        // $favor->save();
        // return $favor;
    }
//    public function challenge(Request $request)
//    {
////        return $this->store($request, Challenge::UPLOADER_CHALLENGER);
//        $challenge = new Challenge($request->all());
//        $challenge->challengerValidation();
//        $challenge->user_id = Auth::user()->id;
//        $challenge->uploader = Challenge::UPLOADER_CHALLENGER;
//
//        if ($challenge->isInvalid()) {
//            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not add Challenge.', $challenge->getErrors());
//        }
//        $challenge->save();
//        if ($request->get('tags')) {
//            $challenge->retag($request->get('tags'));
//        }
//        return $challenge;
//    }

    /**
     * Defend challenge
     *
     * @Post("/defend")
     * 
     * @Parameters({
     *      @Parameter("event_id", type="integer", description="Challenge Id", required=true),
     *      @Parameter("video_url", description="Videos url on AWS S3", required=true)
     * })
     * 
     * @Transaction({
     *      @Request({ "event_id": 1, "video_url": "http://gangster-strength.local.com"}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"event":{"id":1,"title":"Challenge One","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:37","tags":{"100","Pushups"}}}),
     *      @Response(422, body={"message":"Could not reply on Challenge.","errors":{"event_id":{"Invalid Event Id"}},"status_code":422})
     * })
     */
    public function defend(Request $request)
    {
        $event_participant = new EventParticipant($request->all());
        $event_participant->user_id = Auth::user()->id;
        $event_participant->uploader = EventParticipant::UPLOADER_DEFENDER;
        $event_participant->eventIdValidation();
        if ($event_participant->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not add Challenge.', $event_participant->getErrors());
        }
        $event = $event_participant->event;
        if (!$event || !$event->isPending()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not reply on Challenge.', ['event_id' => 'Invalid Event Id']);
        }

        $event->status = Event::STATUS_COMPLETED;
        $event->save();
//        $event_participant->status = EventParticipant::STATUS_COMPLETED;
        $event_participant->save();
        return $event;
    }

    /**
     * Show Challenge Details
     *
     * @Get("/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"event":{"id":2,"title":"Challenge Two","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:42","tags":{"100","Pushups"},"likes":2,"liked":true, "flagged": false,"challenger_video":{"id":2,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":1,"comment":null,"deleted_at":null,"created_at":"2017-04-18 12:58:43","updated_at":"2017-04-18 12:58:43","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}},"defender_video":{"id":4,"event_id":2,"user_id":27,"video_url":"http:\/\/gangster-strength.local.com","status":1,"uploader":2,"comment":null,"deleted_at":null,"created_at":"2017-04-19 07:15:18","updated_at":"2017-04-19 07:15:18","profile":{"name":"Dorris Jakubowski IV","weight":"109.21","height":"176.66","gender":"M","dob":"1993-08-28","biceps":"8.62","shoulders":"45.91","gym_name":"Adams-Smith","avatar":"uploads\/avatars\/M6pYujXszsU3axr6X4IvO73ZrrEF9S18BWPxqsCy.jpeg","ethnicity":207623,"latitude":"74.51276300","longitude":"-154.56846400","description":"WILL do next! As for pulling me out of the leaves: 'I should think you'll feel it a minute or two, she made out what it meant till now.' 'If that's all you know what they're like.' 'I believe so,'."}}}}),
     *      @Response(500, body={"message":"No query results for model [App\\Models\\Challenge] 5","status_code":500})
     * })
     */
    public function show($id)
    {
        $favor = Favor::findOrFail($id);
        return $favor;
    }
//
//    /**
//     * Accept challenge
//     *
//     * @Post("/accept/{id}")
//     * 
//     * @Transaction({
//     *      @Request({"challenge_id": 123}, headers={"Authorization": "Bearer {token}"}),
//     *      @Response(200, body={"challenge":{"title":"User One","description":"I am professioal Builder","type":1,"user_id":21,"created_at":"2017-04-11 12:03:18","id":18,"tags":["Pushups","100"],"video":{"id":18,"challengeable_id":18,"challengeable_type":"App\\Models\\Challenge","video_url":"http:\/\/gangster-strength.local.com","video_size":null,"status":1}}}),
//     *      @Response(500, body={"message":"No query results for model [App\\Models\\Challenge] 5","status_code":500})
//     * })
//     */
//    public function accept($id)
//    {
//        $challenge = Challenge::with('video')->find($id);
//
//        return $challenge;
//    }
//
//    /**
//     * Video Info
//     *
//     * @Post("/{id}")
//     * 
//     * @Parameters({
//     *      @Parameter("video_url", description="Videos url on AWS S3", required=true)
//     * })
//     * 
//     * @Transaction({
//     *      @Request({ "name": "User One", "weight": 111.60, "height": 169.60, "gender": "M", "dob": "1989-05-27", "biceps": 13.4, "shoulders": 16.5, "gym_name": "Best Gym", "avatar": null, "ethnicity": null, "latitude": null, "longitude": null, "description": "I am professioal Builder" }, headers={"Authorization": "Bearer {token}"}),
//     *      @Response(200, body={ "user": { "id": 1, "username": null, "email": null, "created_at": "2017-04-04 11:19:11", "profile": { "name": null, "weight": 111.6, "height": 169.6, "gender": "M", "dob": "1989-05-27", "biceps": null, "shoulders": null, "gym_name": "Best Gym", "avatar": null, "ethnicity": null, "latitude": null, "longitude": null, "description": "I am professional Builder" } } }),
//     *      @Response(422, body={ "message": "Could not update user profile information.", "errors": { "dob": { "The dob is not a valid date." }, "gender": { "The selected gender is invalid." } }, "status_code": 422, })
//     * })
//     */
//    public function update(Request $request, $id)
//    {
//        $challenge = Challenge::with('attack', 'defend')->findOrFail($id);
//        $profile_data = $request->only('weight', 'height', 'gender', 'dob', 'gym_name', 'latitude', 'longitude', 'description');
//        $profile->fill($profile_data);
////        $errors = [];
//        if ($profile->isInvalid()) {
//            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not update user profile information.', $profile->getErrors());
////            $errors = $profile->getErrors();
//        }
////        if ($user->isInvalid()) {
////            $errors = array_merge($user->getErrors(), $errors);
////        }
////        if ($errors) {
////            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not update user profile information.', $errors);
////        }
//        return $user;
//    }

    /**
     * Like an Event/Challenge
     *
     * @Post("/like/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"event":{"id":2,"title":"Challenge Two","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:42","tags":{"100","Pushups"},"likes":2,"liked":true, "flagged": false}}),
     *      @Response(500, body={"message":"No query results for model [App\\Models\\Challenge] 5","status_code":500})
     * })
     */
    public function like(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->likeToggle(); // current user
        return $event;
    }

    /**
     * Flag an Event/Challenge
     *
     * @Post("/flag/{id}")
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={"event":{"id":2,"title":"Challenge Two","type":1,"status":3,"user_id":27,"description":"I am professioal Builder","created_at":"2017-04-18 12:58:42","tags":{"100","Pushups"},"likes":2,"liked":true, "flagged": false}}),
     *      @Response(500, body={"message":"No query results for model [App\\Models\\Challenge] 5","status_code":500})
     * })
     */
    public function flag(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->flagToggle(); // current user
        return $event;
    }



    /**
     * Send friend request
     *
     * @Post("/send-request/{id}")
     * 
     * @Parameters({
     * })
     * 
     * @Transaction({
     *      @Request({}, headers={"Authorization": "Bearer {token}"}),
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not send Friend request.","errors":{"user_id":{"Already Send Request"}},"status_code":422})
     * })
     */
    public function send(Request $request, $id)
    {
        $favor_request = new FavorRequest($request->all());
        $favor_request->doer_id = Auth::user()->id;
        $favor_request->favor_id = $id;
        $favor_request->status = FavorRequest::STATUS_PENDING;
        if ($favor_request->save()) {
            return $favor_request;
        }
        if ($favor_request->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not send favor request.', $favor_request->getErrors());
        }

        throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not send favor request.', $favor_request->getErrors());
        
    }

     /**
     * Accept Friend Request
     *
     * @Post("/accept/{id}")
     * 
     * @Parameters({
     * })
     * 
     * @Transaction({
     *      @Response(200, body={}),
     *      @Response(422, body={"message":"Could not accept Friend request.","errors":{"user_id":{"Request doest not exist."}},"status_code":422})
     * })
     */
    public function accept(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $doer_id = $request->input('doer_id');
        if(!$doer_id){
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not accept favor request.', ['doer_id' => 'Doer is required.']);            
        }
        $favor = Favor::Find($id);
        if (!$favor) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not accept favor request.', ['favor_id' => 'Favor does not exist.']);
        }

        $favor_request = FavorRequest::first();
        // $favor_request->where('status', '=', FavorRequest::STATUS_PENDING);
        $favor_request->where('favor_id', '=', $id);
        $favor_request->where('doer_id', '=', $doer_id);
        $favor_request->get();
        if(!$favor_request){
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not accept favor request.', ['favor_id' => 'Favor request does not exist.']);            
        }
        if($favor_request->status == 2){
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not accept favor request.', ['favor_id' => 'Favor request is already accepted.']);            
        }else if($favor_request->status == 3){
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not accept favor request.', ['favor_id' => 'Favor request is already cancelled.']);                        
        }
        $favor_request->status = FavorRequest::STATUS_ACCEPTED;
        // echo($favor_request->status);die;
        
        $favor->doer_id = $doer_id;
        $favor->status = Favor::STATUS_ACCEPTED;
        $favor_request->save();
        if ($favor->save()) {
            return $favor;
        }
    }

    public function cancel(Request $request, $id)
    {
        $favor_request = FavorRequest::find($id);
        $favor_request->doer_id = Auth::user()->id;
        $favor_request->favor_id = $id;
        $favor_request->status = FavorRequest::STATUS_PENDING;
        if ($favor_request->save()) {
            return $favor_request;
        }
        if ($favor_request->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not cancel favor request.', $favor_request->getErrors());
        }

        throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not cancel favor request.', $favor_request->getErrors());
        
    }
}
