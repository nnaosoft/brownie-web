<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Device;
use App\Helpers\NotificationHelper;

/**
 * @Resource("Devices", uri="/devices" )
 */
class DeviceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    /**
     * Register User
     *
     * @Post("/register")
     * 
     * @Parameters({
     *      @Parameter("device_udid", description="Device UDID", required=true),
     *      @Parameter("firebase_token", type="email", description="Google firebase token", required=true)
     * })
     * 
     * @Transaction({
     *      @Request({ "device_udid": "Device 2", "firebase_token": "token 3"}),
     *      @Response(200, body={"device":{"device_udid":"Device 2","user_id":15,"firebase_token":"token 3","updated_at":"2017-04-27 09:50:33","created_at":"2017-04-27 09:50:33","id":2}}),
     *      @Response(422, body={"message":"Could not register device.","errors":{"device_udid":{"The device udid field is required."},"firebase_token":{"The firebase token field is required."}},"status_code":422})
     * })
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $device_udid = $request->get('device_udid');
        $device = Device::findUDID($device_udid)->first();
        if ($device && $device->user_id != $user->id) {
            $device->user_id = $user->id;
        } else if (!$device) {
            $device = new Device();
            $device->device_udid = $device_udid;
            $device->user_id = $user->id;
        }
        $device->firebase_token = $request->get('firebase_token');
        if ($device->isInvalid()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not register device.', $device->getErrors());
        }
        $device->save();
        return $device;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function notifyMe(Request $request)
    {
        $user = Auth::user();
        $tokens = $user->devices()->pluck('firebase_token')->toArray();
        $message = $request->get('message', "Test Message");
        $body = $request->get('body', 'Test body');
        $data = $request->get('data', []);
        $notification = new NotificationHelper();
        $notification->sendMessage($tokens, $message, $body, $data);
        return $tokens;
    }
}
