<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\Http\Requests\FormRequest;
/**
 * Class RegisterRequest.
 */
class RegisterRequest extends FormRequest
{
  
    protected $message = 'Could not add user.';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => ['required','phone:AUTO',RULE::unique('users')],
            'city_id' => 'required',
        ];
    }


}
