<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
// use Illuminate\Validation\Request;
use App\Http\Requests\FormRequest;
/**
 * Class AddFavorRequest.
 */
class AddFavorRequest extends FormRequest
{
  
    protected $message = 'Could not add favor.';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|min:3',
            'description' => 'required|min:5',
            'image' => 'required|image',
            'points' => 'required',
            'duration' => 'required',
            'phone' => 'required|phone:AUTO',
            'city_id' => 'required',
            // 'status' => 'required',
            'type' => 'required',
            // 'pickup' => 'required',
            // 'dropoff' => 'required',
        ];
        // var_dump($this->get('type'));die;
        if ($this->get('type') == 2 || $this->get('type') == 3) {
            $rules['pickup'] = 'required';
            $rules['dropoff'] = 'required';
        }
        return $rules;
    }


}
