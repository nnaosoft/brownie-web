<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\Http\Requests\FormRequest;
/**
 * Class ResetPasswordRequest.
 */
class ResetPasswordRequest extends FormRequest
{
  
    protected $message = 'Could not reset user password.';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
            'phone' => 'required','phone:AUTO',
            'verification_code' => 'required'
        ];
    }


}
