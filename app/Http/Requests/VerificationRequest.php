<?php

namespace App\Http\Requests;

// use Illuminate\Validation\Rule;
use App\Http\Requests\FormRequest;
/**
 * Class RegisterRequest.
 */
class VerificationRequest extends FormRequest
{
  
    protected $message = 'Could not verify user.';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|phone:AUTO',
            'verification_code' => 'required',
        ];
    }


}
