<?php

namespace App\Http\Requests;

// use Illuminate\Validation\Rule;
use App\Http\Requests\FormRequest;
/**
 * Class RegisterRequest.
 */
class RequestFavorRequest extends FormRequest
{
  
    protected $message = 'Could not request a favor.';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'favor_id' => 'required',
            'seeker_id' => 'required',
            'status' =>  'required'
        ];
    }


}
