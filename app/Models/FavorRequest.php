<?php

namespace App\Models;

use App\Models\AppModel;
// use \Conner\Tagging\Taggable;

class FavorRequest extends AppModel
{

    // use Taggable;

    
    const STATUS_PENDING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELLED = 4;
    // const STATUS_DISPUTED = 5;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'favor_request';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'updated_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = ['challenge_id', 'user_id', 'type', 'uploader', 'status'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

   
}
