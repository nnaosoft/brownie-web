<?php

namespace App\Models;

use App\Models\AppModel as AppModel;

class PasswordReset extends AppModel
{

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'phone' => 'required|phone:AUTO',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'verification_code', 'deleted_at', 'updated_at'
    ];
    
}
