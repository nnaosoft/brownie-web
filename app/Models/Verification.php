<?php

namespace App\Models;

use App\Models\AppModel as AppModel;

class Verification extends AppModel
{
    protected $rules = [
        'phone' => 'required'
    ];
    protected $fillable = [
        'phone'
    ];
    protected $casts = [
//        'schedule_at' => 'datetime',
    ];
    protected $hidden = [
        'user_id'
    ];
    
    public function scopeWherePhone($query, $phone)
    {
        return $query->where('phone', '=', $phone);
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}