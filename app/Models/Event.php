<?php

namespace App\Models;

use App\Models\AppModel;
use App\Models\EventParticipant;
use \Conner\Tagging\Taggable;
use Cog\Likeable\Contracts\HasLikes as HasLikesContract;
use Cog\Likeable\Traits\HasLikes;
use App\Traits\FlaggableTrait;

class Event extends AppModel implements HasLikesContract
{

    use Taggable,
        FlaggableTrait,
        HasLikes;

    const REPLY = 1;
    const LIVE = 2;
    const STATUS_PENDING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_COMPLETED = 3;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'title' => 'required',
        'user_id' => 'required',
        'type' => 'required|in:1,2',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'type', 'status', 'user_id', 'description', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['tagged', 'likesCounter', 'deleted_at', 'updated_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Attributes that get appended on serialization
     *
     * @var array
     */
    protected $appends = [
        'tags',
        'likes',
        'liked',
        'flagged',
    ];

    public function participants()
    {
        return $this->hasMany('App\Models\EventParticipant');
    }

    public function ChallengerVideo()
    {
        return $this->hasOne('App\Models\EventParticipant')
                ->findAttack();
    }

    public function DefenderVideo()
    {
        return $this->hasOne('App\Models\EventParticipant')
                ->findDefend();
    }

    public function getTagsAttribute()
    {
        return $this->tagNames();
    }

    public function getLikesAttribute()
    {
        return $this->likesCount;
    }

    public function getLikedAttribute()
    {
        return $this->liked();
    }

    public function getFlaggedAttribute()
    {
        return $this->flagged();
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', '=', $status);
    }

    public function isLive()
    {
        return ($this->type == self::LIVE) ? true : false;
    }

    public function isReply()
    {
        return ($this->type == self::REPLY) ? true : false;
    }

    public function isPending()
    {
        return ($this->status == self::STATUS_PENDING) ? true : false;
    }

    public function isAccepted()
    {
        return ($this->status == self::STATUS_ACCEPTED) ? true : false;
    }

    public function isCompleted()
    {
        return ($this->status == self::STATUS_COMPLETED) ? true : false;
    }
}
