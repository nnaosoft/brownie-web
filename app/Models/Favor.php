<?php

namespace App\Models;

use App\Models\AppModel;
// use \Conner\Tagging\Taggable;

class Favor extends AppModel
{

    // use Taggable;

    // const REPLY = 1;
    // const LIVE = 2;
    const TYPE_GENERAL = 1;
    const TYPE_DRIVE = 2;
    const TYPE_PICK = 3;
    const STATUS_PENDING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELLED = 4;
    const STATUS_DISPUTED = 5;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'favors';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'points', 'image','duration', 'friend_id', 'phone', 'city_id','type','pickup','dropoff'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'updated_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = ['challenge_id', 'user_id', 'type', 'uploader', 'status'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Attributes that get appended on serialization
     *
     * @var array
     */
//     protected $appends = [
// //        'video_url',
//         'tags',
//     ];

//     public function challengerValidation($add = true)
//     {
//         if ($add) {
//             $this->rules['title'] = 'required';
//             $this->rules['type'] = 'required';
//             if ($this->type == self::REPLY) {
//                 $this->rules['video_url'] = 'required';
//             }
//         } else {
//             unset($this->rules['title']);
//             unset($this->rules['type']);
//             unset($this->rules['video_url']);
//         }
//     }

//     public function defenderValidation($add = true)
//     {
//         if ($add) {
//             $this->rules['challenge_id'] = 'required';
//             $this->rules['video_url'] = 'required';
//         } else {
//             unset($this->rules['challenge_id']);
//             unset($this->rules['video_url']);
//         }
//     }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    // public function Attack()
    // {
    //     return $this->belongsTo('App\Models\Challenge', 'challenge_id');
    // }

    // public function Defend()
    // {
    //     return $this->hasOne('App\Models\Challenge', 'challenge_id');
    // }
    # If one challenge has many replies, for future
//    public function Replies()
//    {
//        return $this->hasMany('App\Models\ChallengeReply');
//    }

    /**
     * Get all of the challenge's videos.
     */
//    public function videos()
//    {
//        return $this->morphMany('App\Models\Video', 'challengeable');
//    }

    /**
     * Get all of the challenge's video.
     */
//    public function video()
//    {
//        return $this->morphOne('App\Models\Video', 'challengeable');
//    }
//
//    public function getVideoUrlAttribute()
//    {
//        return ($this->video) ? $this->video['video_url'] : null;
//    }

    // public function getTagsAttribute()
    // {
    //     return $this->tagNames();
    // }

    // public function scopeStatus($query, $status)
    // {
    //     return $query->where('status', '=', $status);
    // }
}
