<?php

namespace App\Models;

use App\Models\AppModel;
use Auth;

class EventParticipant extends AppModel
{

    const UPLOADER_CHALLENGER = 1;
    const UPLOADER_DEFENDER = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event_participants';

    /**
     * The attributes for validation rules.
     *
     * @var array
     */
    protected $rules = [
        'video_url' => 'required|url',
        'user_id' => 'required',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id', 'user_id', 'video_url', 'status', 'comment', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Attributes that get appended on serialization
     *
     * @var array
     */
    protected $with = [
        'profile',
    ];

    public function videoValidation($add = true)
    {
        if ($add) {
            $this->rules['video_url'] = 'required|url';
        } else {
            unset($this->rules['video_url']);
        }
    }

    public function eventIdValidation($add = true)
    {
        if ($add) {
            $this->rules['event_id'] = 'required';
        } else {
            unset($this->rules['event_id']);
        }
    }

    public function Profile()
    {
        return $this->belongsTo('App\Models\Profile', 'user_id', 'user_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function scopeFindUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

    public function scopeFindAttack($query)
    {
        return $query->where('uploader', self::UPLOADER_CHALLENGER);
    }

    public function scopeFindDefend($query)
    {
        return $query->where('uploader', self::UPLOADER_DEFENDER);
    }

    public function scopeOpponents($query, $events_id, $limit = 5, $user_id = null)
    {
        if (!$user_id) {
            $user_id = Auth::user()->id;
        }
        $query->whereIn('event_id', $events_id)
            ->where('user_id', '<>', $user_id);
    }
}
