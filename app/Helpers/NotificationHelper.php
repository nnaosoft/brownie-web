<?php

namespace App\Helpers;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class NotificationHelper
{

    function sendMessage($tokens, $message, $body = null, $data_payload = array())
    {
        if (empty($tokens) || !$message) {
            return;
        }
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($message);
        if ($body) {
            $notificationBuilder->setBody($notificationBuilder);
        }
        $notificationBuilder->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
//        $dataBuilder->addData(['a_data' => 'my_data']);
        $dataBuilder->addData($data_payload);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

//        $token = [];
//        if (is_array($users)) {
//            $users = [$users];
//        }
//        foreach ($users as $user) {
//            foreach ($user->devices as $device) {
//                $token[] = $device->firebase_token;
//            }
//        }

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
    }
}
