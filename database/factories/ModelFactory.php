<?php
use Illuminate\Support\Facades\Hash;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
//        'password' => $password ?: $password = bcrypt('secret'),
        'password' => Hash::make('123456'),
        'user_type' => App\Models\User::TYPE_NORMAL_USER,
        'status' => $faker->numberBetween(1, 4),
    ];
});

$factory->define(App\Models\Profile::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'height' => $faker->randomFloat(2, 100, 200),
        'weight' => $faker->randomFloat(2, 50, 250),
        'gender' => $faker->randomElement(array("M", "F")),
        'dob' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'biceps' => $faker->randomFloat(2, 5, 50),
        'shoulders' => $faker->randomFloat(2, 5, 50),
        'gym_name' => $faker->company,
        'avatar' => $faker->imageUrl(),
        'ethnicity' => $faker->numberBetween(1, 10),
        'latitude' => $faker->latitude(),
        'longitude' => $faker->longitude(),
        'description' => $faker->realText(),
    ];
});


$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->realText(),
        'description' => $faker->realText(),
        'type' => $faker->numberBetween(1, 2),
        'challenger_id' => $faker->numberBetween(1, 46),
    ];
});
