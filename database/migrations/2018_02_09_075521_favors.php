<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Favors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('image')->nullable()->default(null);
            $table->integer('points')->nullable()->default(null);
            $table->string('duration')->nullable()->default(null);
            $table->integer('user_id')->nullable()->default(null);
            $table->integer('friend_id')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->integer('city_id')->nullable()->default(null);
            $table->integer('doer_id')->nullable()->default(null);
            $table->tinyInteger('type')->default(1);
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('rating')->nullable()->default(null);
            $table->text('pickup')->nullable()->default(null);
            $table->text('dropoff')->nullable()->default(null);
            $table->softDeletes()->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favors');
    }
}
