<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FavorRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favor_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('favor_id')->nullable()->default(null);
            $table->integer('doer_id')->nullable()->default(null);
            // $table->integer('seeker_id')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->softDeletes()->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favor_request');
    }
}
