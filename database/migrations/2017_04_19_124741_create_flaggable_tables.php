<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlaggableTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flaggable_flags', function(Blueprint $table) {
            $table->increments('id');
            $table->string('flaggable_id', 36);
            $table->string('flaggable_type', 255);
            $table->integer('user_id')->index();
            $table->timestamps();
            $table->unique(['flaggable_id', 'flaggable_type', 'user_id'], 'flaggable_flags_unique');
        });

        Schema::create('flaggable_flag_counters', function(Blueprint $table) {
            $table->increments('id');
            $table->string('flaggable_id', 36);
            $table->string('flaggable_type', 255);
            $table->integer('count')->unsigned()->default(0);
            $table->unique(['flaggable_id', 'flaggable_type'], 'flaggable_counts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flaggable_flags');
        Schema::drop('flaggable_flag_counters');
    }
}
