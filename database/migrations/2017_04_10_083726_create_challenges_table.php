<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('challenges', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id')->unsigned();
//            $table->string('title')->nullable()->default(null);
//            $table->tinyInteger('type'); # live or reply
//            $table->tinyInteger('uploader'); # challenger or defender
//            $table->integer('challenge_id')->unsigned()->nullable()->default(null);
//            $table->unsignedTinyInteger('status')->default(1);
//            $table->string('video_url')->nullable()->default(null);
//            $table->text('description')->nullable()->default(null);
//            $table->softDeletes()->nullable()->default(null);
//            $table->timestamps();
//        });

//        Schema::create('challenges', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id')->unsigned();
//            $table->string('title');
//            $table->tinyInteger('type'); # live or reply
//            $table->unsignedTinyInteger('status')->default(1);
////            $table->string('video_url')->nullable()->default(null);
//            $table->text('description')->nullable()->default(null);
//            $table->softDeletes()->nullable()->default(null);
//            $table->timestamps();
//        });
//
//        Schema::create('challenge_replies', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id')->unsigned();
//            $table->integer('challenge_id')->unsigned();
//            $table->string('comment')->nullable()->default(null);
////            $table->string('video_url')->nullable()->default(null);
//            $table->text('description')->nullable()->default(null);
//            $table->softDeletes()->nullable()->default(null);
//            $table->timestamps();
//        });
//
//        Schema::create('videos', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('challengeable_id')->unsigned();   # id of reply or challenge table
//            $table->string('challengeable_type');   # model 
//            $table->string('video_url')->nullable()->default(null);
//            $table->string('video_size')->nullable()->default(null);
//            $table->unsignedTinyInteger('status')->default(1);
//            $table->softDeletes()->nullable()->default(null);
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('challenges');
//        Schema::dropIfExists('challenge_replies');
//        Schema::dropIfExists('videos');
    }
}
