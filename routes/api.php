<?php
use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$api = app('Dingo\Api\Routing\Router');

# Public Actions
$api->version('v1', function ($api) {
    $api->post('users/register', 'App\Http\Controllers\UserController@store');
    $api->post('users/verify', 'App\Http\Controllers\UserController@verify');
//        $api->post('users/login/{provider}', 'App\Http\Controllers\UserController@login');
    $api->post('users/login/', 'App\Http\Controllers\UserController@login');
    // $api->post('users/login/google', 'App\Http\Controllers\UserController@google');
    // $api->post('users/login/facebook', 'App\Http\Controllers\UserController@facebook');
    // $api->post('users/login/twitter', 'App\Http\Controllers\UserController@twitter');
    $api->post('users/send-password-reset-code', 'App\Http\Controllers\UserController@sendPasswordResetCode');
    $api->post('users/reset-password', 'App\Http\Controllers\UserController@setPassword');
    $api->post('users/resend-code', 'App\Http\Controllers\UserController@resendCode');
});

# Private Actions
$api->version('v1', ['middleware' => 'jwt.auth'], function ($api) {

    $api->get('favors/addedbyme','App\Http\Controllers\FavorController@favorsAddedByMe');
    $api->get('favors/donebyme','App\Http\Controllers\FavorController@favorsDoneByMe');
    $api->resource('favors', 'App\Http\Controllers\FavorController');
    $api->post('favor/send_request/{id}', 'App\Http\Controllers\FavorController@send');
    $api->post('favor/accept/{id}', 'App\Http\Controllers\FavorController@accept');
    $api->resource('users', 'App\Http\Controllers\UserController');
//        $api->post('users/generate-random-password', 'App\Http\Controllers\UserController@generateRandomPassword');
    $api->post('users/avatar', 'App\Http\Controllers\UserController@avatar');
    $api->post('users/me', 'App\Http\Controllers\UserController@update');
    // $api->get('users/videos/{id}', 'App\Http\Controllers\UserController@videos');
    # Challenges
    // $api->get('challenges/timeline', 'App\Http\Controllers\ChallengeController@timeline');
    // $api->resource('challenges', 'App\Http\Controllers\ChallengeController');
    // $api->post('challenges/challenge', 'App\Http\Controllers\ChallengeController@challenge');
    // $api->post('challenges/defend', 'App\Http\Controllers\ChallengeController@defend');
    // $api->post('challenges/{id}', 'App\Http\Controllers\ChallengeController@update');
    // $api->post('challenges/like/{id}', 'App\Http\Controllers\ChallengeController@like');
    // $api->post('challenges/flag/{id}', 'App\Http\Controllers\ChallengeController@flag');
    // $api->post('challenges/accept/{id}', 'App\Http\Controllers\ChallengeController@accept');
    #Friends
//    $api->resource('friends', 'App\Http\Controllers\FriendController');
    $api->get('friends', 'App\Http\Controllers\FriendController@index');
    $api->get('friends/pending', 'App\Http\Controllers\FriendController@pending');
    $api->get('friends/request', 'App\Http\Controllers\FriendController@request');
    $api->post('friends/send-request/{id}', 'App\Http\Controllers\FriendController@send');
    $api->post('friends/accept/{id}', 'App\Http\Controllers\FriendController@accept');
    $api->post('friends/deny/{id}', 'App\Http\Controllers\FriendController@deny');
    $api->post('friends/remove/{id}', 'App\Http\Controllers\FriendController@remove');
    $api->post('friends/block/{id}', 'App\Http\Controllers\FriendController@block');
    $api->post('friends/unblock/{id}', 'App\Http\Controllers\FriendController@unblock');
    #Devices
    $api->post('devices/register', 'App\Http\Controllers\DeviceController@store');
    $api->post('devices/notify-me', 'App\Http\Controllers\DeviceController@notifyMe');
    #Messages
    $api->get('messages/unread-messages-count', 'App\Http\Controllers\MessageController@UnreadMessagesCount');
    $api->post('messages/read-thread', 'App\Http\Controllers\MessageController@ReadThread');
    $api->resource('messages', 'App\Http\Controllers\MessageController');
});
